<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Peminjaman extends Model
{
    protected $fillable = ['tgl_pinjam', 'tgl_batasakhir', 'tgl_kembali', 'user_id', 'buku_id','ontime'];

    public function mahasiswa(){
        return $this->belongsTo(Mahasiswa::class);
    }
}
