<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Mahasiswa extends Model
{
    protected $fillable = ['nama', 'nim', 'fakultas', 'jurusan', 'no_hp', 'no_wa'];

    public function getRouteKeyName()
    {
        return 'nim';
    }

    public function user(){
        return $this->hasOne(User::class);
    }

    public function peminjaman(){
        return $this->hasMany(Peminjaman::class);
    }
}
