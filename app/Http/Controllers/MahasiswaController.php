<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Mahasiswa;
use App\Http\Resources\MahasiswaResource;

class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mahasiswa = Mahasiswa::get();
        return MahasiswaResource::collection($mahasiswa);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'nama' => 'required',
            'nim' => ['required', 'unique:mahasiswas,nim'],
            'fakultas' => 'required',
            'jurusan' => 'required',
            'no_hp' => ['numeric', 'required'],
            'no_wa' => ['numeric', 'required']
        ]);

        $mahasiswa = auth()->user()->mahasiswa()->create($this->storeMahasiswa());

        return $mahasiswa;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($nim)
    {
        $mahasiswa = Mahasiswa::where('nim', $nim)->get();
        return new MahasiswaResource($mahasiswa);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $nim)
    {
        request()->validate([
            'nama' => 'required',
            'nim' => 'required',
            'fakultas' => 'required',
            'jurusan' => 'required',
            'no_hp' => ['numeric', 'required'],
            'no_wa' => ['numeric', 'required']
        ]);

        auth()->user()->mahasiswa()->update($this->storeMahasiswa());

        $mahasiswa = Mahasiswa::where('nim', $nim)->get();
        return $mahasiswa;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($nim)
    {
        Mahasiswa::where('nim', $nim)->delete();

        return response('Data Mahasiswa telah dihapus', 200);
    }

    public function storeMahasiswa(){
        return [
            'nama' => request('nama'),
            'nim' => request('nim'),
            'fakultas' => request('fakultas'),
            'jurusan' => request('jurusan'),
            'no_hp' => request('no_hp'),
            'no_wa' => request('no_wa')
        ];
    }
}
