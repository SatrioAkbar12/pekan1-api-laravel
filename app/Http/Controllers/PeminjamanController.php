<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use App\Model\Peminjaman;
use App\Http\Resources\PeminjamanResource;

class PeminjamanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($user_id)
    {
        $peminjaman = Peminjaman::where('user_id', $user_id)->get();

        return PeminjamanResource::collection($peminjaman);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'buku_id' => 'required'
        ]);

        $pinjam = auth()->user()->peminjaman()->create([
            'tgl_pinjam' => date('Y-m-d H:i:s'),
            'tgl_batasakhir' => date('Y-m-d H:i:s', strtotime('+1 Week')),
            'buku_id' => request('buku_id'),
            'ontime' => false
        ]);

        return $pinjam;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($user_id, $id)
    {
        $peminjaman = Peminjaman::find($id);
        return new PeminjamanResource($peminjaman);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $user_id, $id)
    {
        request()->validate([
            'tgl_pinjam' => ['date', 'required'],
            'tgl_batasakhir' => ['date', 'required'],
            'tgl_kembali' => ['date', 'required'],
            'user_id' => 'required',
            'buku_id' => 'required'
        ]);

        $peminjaman = Peminjaman::find($id);
        $ontime = false;
        if($peminjaman->tgl_batasakhir >= request('tgl_kembali')){
            $ontime = true;
        }

        Peminjaman::where('id', $id)->update([
            'tgl_pinjam' => request('tgl_pinjam'),
            'tgl_batasakhir' => request('tgl_batasakhir'),
            'tgl_kembali' => request('tgl_kembali'),
            'user_id' => request('user_id'),
            'buku_id' => request('buku_id'),
            'ontime' => $ontime
        ]);

        return Peminjaman::find($id);
    }

    public function kembali($id)
    {
        $peminjaman = Peminjaman::find($id);
        $ontime = false;

        if($peminjaman->tgl_batasakhir >= date('Y-m-d H:i:s')){
            $ontime = true;
        }

        Peminjaman::where('id', $id)->update([
            'tgl_kembali' => date('Y-m-d H:i:s'),
            'ontime' => $ontime
        ]);

        return Peminjaman::find($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($user_id, $id)
    {
        Peminjaman::find($id)->delete();

        return response('Data Peminjaman telah berhasil dihapus', 200);
    }
}
