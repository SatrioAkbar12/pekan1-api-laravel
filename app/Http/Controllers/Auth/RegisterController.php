<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        request()->validate([
            'email' => ['email', 'required', 'unique:users,email'],
            'password' => ['required']
        ]);

        User::create([
            'email' => request('email'),
            'password' => Hash::make(request('password'))
        ]);

        return response('Terima kasih, anda telah terdaftar');
    }

}
