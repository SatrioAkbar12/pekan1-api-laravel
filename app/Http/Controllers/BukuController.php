<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Buku;
use App\Http\Resources\BukuResource;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buku = Buku::get();
        return BukuResource::collection($buku);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'kode_buku' => ['required', 'unique:bukus,kode_buku'],
            'judul' => 'required',
            'pengarang' => 'required',
            'tahun_terbit' => 'required'
        ]);

        $buku = Buku::create($this->storeBuku());

        return $buku;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($kode)
    {
        return new BukuResource(Buku::where('kode_buku',$kode)->get());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $kode)
    {
        request()->validate([
            'kode_buku' => ['required'],
            'judul' => 'required',
            'pengarang' => 'required',
            'tahun_terbit' => 'required'
        ]);

        Buku::where('kode_buku', $kode)->update($this->storeBuku());
        $buku = Buku::where('kode_buku',$kode)->get();

        return new BukuResource($buku);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($kode)
    {
        Buku::where('kode_buku', $kode)->delete();

        return response()->json('Buku telah dihapus', 200);
    }

    public function storeBuku(){
        return [
            'kode_buku' => request('kode_buku'),
            'judul' => request('judul'),
            'pengarang' => request('pengarang'),
            'tahun_terbit' => request('tahun_terbit')
        ];
    }
}
