<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('/register', 'Auth\RegisterController')->name('register');
Route::post('/login', 'Auth\LoginController')->name('login');
Route::post('/logout', 'Auth\LogoutController')->name('logout');

//Group middleware auth
Route::middleware('auth:api')->group(function() {
    Route::get('/home', 'HomeController');

    Route::get('/buku', 'BukuController@index');
    Route::get('/buku/{kode_buku}', 'BukuController@show');

    Route::get('/mahasiswa', 'MahasiswaController@index');
    Route::get('/mahasiswa/{nim}', 'MahasiswaController@show');
    Route::post('/mahasiswa/store', 'MahasiswaController@store');
    Route::patch('/mahasiswa/update/{nim}', 'MahasiswaController@update');
    Route::delete('/mahasiswa/delete/{nim}', 'MahasiswaController@destroy');

    Route::get('/peminjaman/{user_id}', 'PeminjamanController@index');
    Route::get('/peminjaman/{user_id}/{id}', 'PeminjamanController@show');
    Route::post('/peminjaman/create', 'PeminjamanController@store');
    Route::patch('/peminjaman/{user_id}/kembali', 'PeminjamanController@kembali')->name('peminjaman.kembali');
});

//Group middleware auth dan admin
Route::middleware('auth:api','admin')->group(function(){
    Route::post('/buku/store', 'BukuController@store');
    Route::patch('/buku/update/{kode_buku}', 'BukuController@update');
    Route::delete('/buku/delete/{kode_buku}', 'BukuController@destroy');

    Route::put('/peminjaman/{user_id}/{id}/update', 'PeminjamanController@update')->name('peminjaman.update');
    Route::delete('/peminjaman/{user_id}/{id}/delete', 'PeminjamanController@destroy');
});
